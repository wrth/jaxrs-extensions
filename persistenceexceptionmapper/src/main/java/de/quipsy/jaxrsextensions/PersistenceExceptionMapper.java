/*
 * Copyright (C) 2017 Team QUIPSY
 */

package de.quipsy.jaxrsextensions;

import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * JAX-RS Exception Mapper for Persistence Exception
 *
 * @author Ralf HAMBERGER (hamberger@quipsy.de)
 * @author Markus KARG (karg@quipsy.de)
 */
@Provider
public final class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {

    @Override
    public final Response toResponse(final PersistenceException e) {
        if (e instanceof OptimisticLockException)
            return Response.status(CONFLICT).build();

        if (e instanceof NoResultException) {
            if (e.getMessage() != null)
                return Response.status(NOT_FOUND).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();

            return Response.status(NOT_FOUND).build();
        }

        return Response.serverError().build();
    }

}
