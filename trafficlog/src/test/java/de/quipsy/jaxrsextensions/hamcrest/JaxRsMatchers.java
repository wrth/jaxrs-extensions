/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.jaxrsextensions.hamcrest;

import static com.natpryce.hamcrest.reflection.HasAnnotationMatcher.hasAnnotation;
import static com.natpryce.hamcrest.reflection.ModifierMatcher.isPublic;
import static de.quipsy.hamcrest.ConstructorMatcher.constructor;
import static de.quipsy.hamcrest.Type.type;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;

import java.lang.reflect.AnnotatedElement;

import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Feature;
import javax.ws.rs.ext.Provider;

import de.quipsy.hamcrest.Type;

import org.hamcrest.Matcher;

public final class JaxRsMatchers {

	public static final <T> Matcher<T> isAutomaticallyDiscoverableProvider() {
		return allOf(isProvider(), classHasProviderAnnotation());
	}

	public static final <T> Matcher<T> isProvider() {
		return classHasPublicConstructor();
	}

	private static final <T> Matcher<T> classHasPublicConstructor() {
		return type(constructor(isPublic()));
	}

	private static final <T> Type<T> classHasProviderAnnotation() {
		return type(hasProviderAnnotation());
	}

	private static final <T> Matcher<AnnotatedElement> hasProviderAnnotation() {
		return hasAnnotation(Provider.class);
	}

	public static final <T> Matcher<T> isFeature() {
		return instanceOf(Feature.class);
	}

	public static final <T> Matcher<T> isContainerRequestFilter() {
		return instanceOf(ContainerRequestFilter.class);
	}

	public static final <T> Matcher<T> isContainerResponseFilter() {
		return instanceOf(ContainerResponseFilter.class);
	}

	public static final <T> Matcher<T> isClientRequestFilter() {
		return instanceOf(ClientRequestFilter.class);
	}

	public static final <T> Matcher<T> isClientResponseFilter() {
		return instanceOf(ClientResponseFilter.class);
	}
}
