/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.jaxrsextensions.trafficlog;

import static de.quipsy.jaxrsextensions.hamcrest.JaxRsMatchers.isAutomaticallyDiscoverableProvider;
import static de.quipsy.jaxrsextensions.hamcrest.JaxRsMatchers.isClientRequestFilter;
import static de.quipsy.jaxrsextensions.hamcrest.JaxRsMatchers.isClientResponseFilter;
import static de.quipsy.jaxrsextensions.hamcrest.JaxRsMatchers.isContainerRequestFilter;
import static de.quipsy.jaxrsextensions.hamcrest.JaxRsMatchers.isContainerResponseFilter;
import static java.util.logging.Level.FINE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public final class TrafficLogTest {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule().silent();

    @Test
    public final void shouldBeAutomaticallyDiscoverableFilter() {
        assertThat(new TrafficLog(), allOf(isAutomaticallyDiscoverableProvider(), isContainerRequestFilter(),
                isContainerResponseFilter(), isClientRequestFilter(), isClientResponseFilter()));
    }

    @Mock
    private Configuration configuration;

    @Mock
    private ContainerRequestContext containerRequestContext;

    @Mock
    private ContainerResponseContext containerResponseContext;

    @Mock
    private ClientRequestContext clientRequestContext;

    @Mock
    private ClientResponseContext clientResponseContext;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private Handler handler;

    @InjectMocks
    private TrafficLog trafficLog;

    private Logger logger;

    @Before
    public final void setup() {
        this.logger = Logger.getLogger(TrafficLogTest.class.getName());

        given(this.configuration.getProperty(TrafficLog.PROPERTY_LOGGER)).willReturn(this.logger);
        given(this.uriInfo.toString()).willReturn("URL");

        given(this.containerRequestContext.getMethod()).willReturn("METHOD");
        given(this.containerRequestContext.getUriInfo()).willReturn(this.uriInfo);

        given(this.containerResponseContext.getStatus()).willReturn(200);
        given(this.containerResponseContext.getStatusInfo()).willReturn(Status.OK);

        given(this.clientRequestContext.getMethod()).willReturn("METHOD");
        given(this.clientRequestContext.getUri()).willReturn(URI.create("SOME:URI"));
        given(this.clientRequestContext.getHeaderString(HttpHeaders.CONTENT_LENGTH)).willReturn("0");

        given(this.clientResponseContext.getStatus()).willReturn(200);
        given(this.clientResponseContext.getStatusInfo()).willReturn(Status.OK);

        this.logger.addHandler(this.handler);
        this.logger.setLevel(FINE);
    }

    @After
    public final void tearDown() {
        this.logger = null;
    }

    @Test
    public final void shouldLogContainerRequest() throws IOException {
        // given
        final ContainerRequestFilter requestFilter = this.trafficLog;

        // when
        requestFilter.filter(this.containerRequestContext);

        // then
        final ArgumentCaptor<LogRecord> argumentCaptor = ArgumentCaptor.forClass(LogRecord.class);
        verify(this.handler).publish(argumentCaptor.capture());
        final String capturedMessage = argumentCaptor.getAllValues().get(0).getMessage();
        assertThat(capturedMessage, is("(IN) REQUEST: METHOD URL (null) [0]"));
    }

    @Test
    public final void shouldLogContainerResponse() throws IOException {
        // given
        final ContainerResponseFilter responseFilter = trafficLog;

        // when
        responseFilter.filter(this.containerRequestContext, this.containerResponseContext);

        // then
        final ArgumentCaptor<LogRecord> argumentCaptor = ArgumentCaptor.forClass(LogRecord.class);
        verify(this.handler).publish(argumentCaptor.capture());
        final String capturedMessage = argumentCaptor.getAllValues().get(0).getMessage();
        assertThat(capturedMessage, is("(OUT) RESPONSE: 200 OK (null) [0]"));
    }

    @Test
    public final void shouldLogClientRequest() throws IOException {
        // given
        final ClientRequestFilter requestFilter = this.trafficLog;

        // when
        requestFilter.filter(this.clientRequestContext);

        // then
        final ArgumentCaptor<LogRecord> argumentCaptor = ArgumentCaptor.forClass(LogRecord.class);
        verify(this.handler).publish(argumentCaptor.capture());
        final String capturedMessage = argumentCaptor.getAllValues().get(0).getMessage();
        assertThat(capturedMessage, is("(OUT) REQUEST: METHOD SOME:URI (null) [0]"));
    }

    @Test
    public final void shouldLogClientResponse() throws IOException {
        // given
        final ClientResponseFilter responseFilter = trafficLog;

        // when
        responseFilter.filter(this.clientRequestContext, this.clientResponseContext);

        // then
        final ArgumentCaptor<LogRecord> argumentCaptor = ArgumentCaptor.forClass(LogRecord.class);
        verify(this.handler).publish(argumentCaptor.capture());
        final String capturedMessage = argumentCaptor.getAllValues().get(0).getMessage();
        assertThat(capturedMessage, is("(IN) RESPONSE: 200 OK (null) [0]"));
    }
}
