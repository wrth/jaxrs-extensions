/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.hamcrest;

import static com.natpryce.hamcrest.reflection.ModifierMatcher.isFinal;
import static com.natpryce.hamcrest.reflection.ModifierMatcher.isPublic;
import static de.quipsy.hamcrest.ConstructorMatcher.constructor;
import static java.lang.Boolean.FALSE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.junit.Test;

public final class ConstructorMatcherTest {

	@Test
	public final void shouldBeMatcher() {
		assertThat(constructor(null), is(instanceOf(Matcher.class)));
	}

	@Test
	public final void shouldApplyProvidedModifiers() {
		// given
		final Matcher<?> matcher = constructor(isFinal());

		// when
		final boolean result = matcher.matches(Object.class);

		// then
		assertThat(result, is(FALSE));
	}

	@Test
	public final void shouldDescribeModifiers() {
		// given
		final Matcher<?> matcher = constructor(isPublic());
		final Description description = new StringDescription();

		// when
		matcher.describeTo(description);

		// then
		assertThat(description.toString(), is("constructor is public"));
	}

	@Test
	public final void shouldDescribeModifiersMismatch() {
		// given
		final Matcher<?> matcher = constructor(isPublic());
		final Description description = new StringDescription();

		// when
		matcher.describeMismatch(null, description);

		// then
		assertThat(description.toString(), is("no constructor is public"));
	}
}
