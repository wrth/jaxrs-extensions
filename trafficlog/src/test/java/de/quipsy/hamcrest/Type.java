/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.hamcrest;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

/**
 * Converts matchers for classes into matchers for instances of these classes.
 *
 * @author Markus KARG (markus@headcrashing.eu)
 */
public final class Type<T> extends BaseMatcher<T> {

	public static final <T> Type<T> type(final Matcher<? super Class<T>> matcher) {
		return new Type<>(matcher);
	}

	private Type(final Matcher<? super Class<T>> matcher) {
		this.matcher = matcher;
	}

	private final Matcher<? super Class<T>> matcher;

	@Override
	public final boolean matches(final Object item) {
		return this.matcher.matches(item.getClass());
	}

	@Override
	public final void describeTo(final Description description) {
		description.appendDescriptionOf(this.matcher);
	}

	@Override
	public final void describeMismatch(final Object item, final Description description) {
		this.matcher.describeMismatch(item.getClass(), description);
	}

}
