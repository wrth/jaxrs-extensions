/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.hamcrest;

import java.lang.reflect.Constructor;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import com.natpryce.hamcrest.reflection.Reflectomatic;

public class ConstructorMatcher extends BaseMatcher<Class<?>> {
	private final Matcher<? super Constructor<?>> modifierMatcher;

	private ConstructorMatcher(final Matcher<? super Constructor<?>> modifierMatcher) {
		this.modifierMatcher = modifierMatcher;
	}

	@Override
	public final boolean matches(final Object item) {
		return !Reflectomatic.constructorsOf((Class<?>) item, this.modifierMatcher).isEmpty();
	}

	@Override
	public final void describeTo(final Description description) {
		description.appendText("constructor ").appendDescriptionOf(this.modifierMatcher);
	}

	@Override
	public void describeMismatch(final Object item, final Description mismatchDescription) {
		mismatchDescription.appendText("no ");
		this.describeTo(mismatchDescription);
	}

	public static final ConstructorMatcher constructor(final Matcher<? super Constructor<?>> modifierMatcher) {
		return new ConstructorMatcher(modifierMatcher);
	}

}
