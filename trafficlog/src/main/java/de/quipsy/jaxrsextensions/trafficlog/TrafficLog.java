/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.jaxrsextensions.trafficlog;

import static javax.ws.rs.core.HttpHeaders.CONTENT_LENGTH;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * Logs incoming and outgoing traffic.
 * 
 * The {@link Logger logger} by default is named
 * {@code de.quipsy.jaxrsextensions.trafficlog.TrafficLog} by default, but can
 * get replaced by setting a logger in the property {@link #PROPERTY_LOGGER}.
 * 
 * @author Markus KARG (karg@quipsy.de)
 */
@Provider
public final class TrafficLog
        implements ContainerRequestFilter, ContainerResponseFilter, ClientRequestFilter, ClientResponseFilter {

    /**
     * Allows replacing the logger.
     */
    public static final String PROPERTY_LOGGER = TrafficLog.class.getName() + "/Logger";

    private static final Logger LOGGER = Logger.getLogger(TrafficLog.class.getName());

    @Context
    private Configuration config;

    private Logger logger;

    private final Logger logger() {
        if (this.logger == null) {
            this.logger = config.getProperty(PROPERTY_LOGGER) instanceof Logger
                    ? (Logger) config.getProperty(PROPERTY_LOGGER)
                    : LOGGER;
            LOGGER.config("Configured traffic log");
        }
        return this.logger;
    }

    @Override
    public final void filter(final ContainerRequestContext requestContext) {
        final Logger logger = this.logger();
        if (logger.isLoggable(Level.FINE))
            logger.fine(String.format("(IN) REQUEST: %s %s (%s) [%s]", requestContext.getMethod(),
                    requestContext.getUriInfo(), requestContext.getHeaders(), requestContext.getLength()));
    }

    @Override
    public final void filter(final ContainerRequestContext requestContext,
            final ContainerResponseContext responseContext) {
        final Logger logger = this.logger();
        if (logger.isLoggable(Level.FINE))
            logger.fine(String.format("(OUT) RESPONSE: %s %s (%s) [%s]", responseContext.getStatus(),
                    responseContext.getStatusInfo().getReasonPhrase(), responseContext.getHeaders(),
                    responseContext.getLength()));
    }

    @Override
    public final void filter(final ClientRequestContext requestContext) {
        final Logger logger = this.logger();
        if (logger.isLoggable(Level.FINE))
            logger.fine(
                    String.format("(OUT) REQUEST: %s %s (%s) [%s]", requestContext.getMethod(), requestContext.getUri(),
                            requestContext.getHeaders(), requestContext.getHeaderString(CONTENT_LENGTH)));
    }

    @Override
    public final void filter(final ClientRequestContext requestContext, final ClientResponseContext responseContext) {
        final Logger logger = this.logger();
        if (logger.isLoggable(Level.FINE))
            logger.fine(String.format("(IN) RESPONSE: %s %s (%s) [%s]", responseContext.getStatus(),
                    responseContext.getStatusInfo().getReasonPhrase(), responseContext.getHeaders(),
                    responseContext.getLength()));
    }
}
